# -*- encoding: utf-8 -*-
import operator

def count_relevant(database, logging):
    logging.info("Count Relevant")
    dict_result = {}
    bad_list = ['', ' ', 622]
    try:
        sql_statement = """
                        SELECT user_id, GROUP_CONCAT(store_content_id) as list_books
                        FROM user_content
                        JOIN content_store ON user_content.store_content_id = content_store.id
                        JOIN content_price ON content_price.content_id = content_store.id
                        JOIN content_bookmark ON content_bookmark.content_id = user_content.user_id
                        WHERE content_store.status = 1
                            AND user_content.store_content_id NOT IN ('',' ')
                            AND content_price.price != '0'
                            AND user_content.status = 1
                            AND content_bookmark.data like '%"paragraph_index":%'
                        GROUP BY user_id HAVING list_books NOT IN ('', ' ')
                        """
        list_readed_books = database.execute_select(sql_statement=sql_statement)

        for book in list_readed_books:

            # split and remove duplicate in list book ids from user_content
            list_id = list(set([book_id for book_id in book[1].split(',') if book_id not in bad_list]))

            # Move to couple (i,j)
            couple_list_id = [(i, j) for i in list_id for j in list_id if i != j]

            # Count Relevant Books for each Book
            for couple in couple_list_id:
                if not dict_result.get(couple[0]):  dict_result[couple[0]] = {}
                if not dict_result.get(couple[1]):  dict_result[couple[1]] = {}
                dict_result[couple[0]][couple[1]] = dict_result[couple[0]].get(couple[1],False) and dict_result[couple[0]][couple[1]] + 1 or 1
                dict_result[couple[1]][couple[0]] = dict_result[couple[1]].get(couple[0],False) and dict_result[couple[1]][couple[0]] + 1 or 1

    except Exception as e:
        logging.error("Failed: Count Relevant")
        logging.error(e)
    return dict_result

def sort_relevant(data={}, logging=None):
    logging.info("Sort Relevant")
    try:
        for key, value in data.iteritems():
            data[key] = sorted(data[key].items(), key=operator.itemgetter(1), reverse=True)
    except Exception as e:
        logging.error("Failed: Sort Relevant")
        logging.error(e)
    return data

def normalize_data(data={}, logging=None):
    logging.info("Normalize Data")
    try:
        for key, value in data.iteritems():
            temp_book_list = []
            for book_tuple in data[key][0:20]:
                temp_book_list.append(book_tuple[0])
            data[key] = temp_book_list
    except Exception as e:
        logging.error("Failed: Normalize Data")
        logging.error(e)
    return data

def insert_to_mysql(database, data={}, logging=None):
    logging.info("Insert Data to DB")
    result = False
    try:
        result = database.insert_multi_relevant(data)
    except Exception as e:
        logging.error("Failed: Insert Data to DB")
        logging.error(e)
    return result

def delete_relevant(database, logging=None):
    logging.info("Delete Relevant in Database")
    result = False
    try:
        result = database.remove_relevant()
    except Exception as e:
        logging.error("Failed: Delete Relevant in Database")
        logging.error(e)
    return result
