# -*- encoding: utf-8 -*-

class Config(object):
    ###Logging file for logging API-SERVICE
    LOGGING_FILE = ''

    ### DB Connection to DB
    DATABASE_USER = ''
    DATABASE_PASSWORD = ''
    DATABASE_DB = ''
    DATABASE_HOST = ''

class ProductionConfig(Config):
    LOGGING_FILE = '/var/log/supporting-recommend.log'

    DATABASE_USER = ''
    DATABASE_PASSWORD = ''
    DATABASE_DB = ''
    DATABASE_HOST = ''

class StagingConfig(Config):
    LOGGING_FILE = '/var/log/supporting-recommend.log'

    DATABASE_USER = ''
    DATABASE_PASSWORD = ''
    DATABASE_DB = ''
    DATABASE_HOST = ''

class DevelopmentConfig(Config):
    LOGGING_FILE = '/var/log/supporting-recommend.log'

    DATABASE_USER = ''
    DATABASE_PASSWORD = ''
    DATABASE_DB = ''
    DATABASE_HOST = ''
