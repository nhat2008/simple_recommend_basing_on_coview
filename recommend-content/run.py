# -*- encoding: utf-8 -*-
from recommends.book import *
import config
from dbs import mysql as mysql
import os
import logging
import sys

def connect_db(*args, **kw):
    """Connects to the specific database."""
    db_mysql = mysql.MySQL(*args, **kw)
    db_mysql.connection()
    return db_mysql

if __name__ == '__main__':
    # Tool config
    app_config = config.StagingConfig
    if os.environ.get('SUPPORTING', False):
        if os.environ.get('SUPPORTING', False) == 'Development':
            app_config = config.DevelopmentConfig
        if os.environ.get('SUPPORTING', False) == 'Staging':
            app_config = config.StagingConfig
        if os.environ.get('SUPPORTING', False) == 'Production':
            app_config = config.ProductionConfig

    logging.basicConfig(filename=app_config.LOGGING_FILE, level=logging.INFO)
    logging.info("Start: Recommend Job")

    DATABASE_USER = app_config.DATABASE_USER
    DATABASE_PASSWORD = app_config.DATABASE_PASSWORD
    DATABASE_DB = app_config.DATABASE_DB
    DATABASE_HOST = app_config.DATABASE_HOST

    input_args = sys.argv
    if len(input_args) > 4:
        DATABASE_HOST = input_args[1]
        DATABASE_USER = input_args[2]
        DATABASE_PASSWORD = input_args[3]
        DATABASE_DB = input_args[4]

    LOGGING_FILE = app_config.LOGGING_FILE

    database = connect_db(host=DATABASE_HOST, user=DATABASE_USER, passwd=DATABASE_PASSWORD, schema=DATABASE_DB)

    delete = delete_relevant(database, logging)
    result = count_relevant(database, logging)
    result = sort_relevant(result, logging)
    result = normalize_data(result, logging)
    result = insert_to_mysql(database,result, logging)
    database.close()
