# -*- encoding: utf-8 -*-
import MySQLdb
import abc

class Database(object):

    @abc.abstractmethod
    def __init__(self, *args, **kw):
        pass

    @abc.abstractmethod
    def connection(self, *args, **kw):
        pass


class MySQL(Database):
    """MYSQL CLASS"""
    def __init__(self, *args, **kw):
        self.db = None
        self.cur = None
        self.host = kw.get('host',False)
        self.user = kw.get('user',False)
        self.passwd = kw.get('passwd',False)
        self.schema = kw.get('schema',False)

    def connection(self, *args, **kw):
        if self.host and self.user and self.passwd and self.schema:
            self.db = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd, db=self.schema)
            return True
        return False

    def close(self, *args, **kw):
        if self.db:
            self.db.close()

    def execute_insert(self, sql_statement=''):
        result = False
        try:
            with self.db.cursor() as cur:
                stmt = sql_statement
                # int(kw['tiki_product_id']),
                result = cur.execute(stmt)
                self.db.commit()
        except Exception as e:
            self.db.rollback()
        return result

    def execute_select(self, sql_statement=''):
        result = False
        try:
            cur = self.db.cursor()
            stmt = sql_statement
            cur.execute(stmt)
            result = cur.fetchall()
        except Exception as e:
            pass
        return result

    def remove_relevant(self):
        result = False
        try:
            cur = self.db.cursor()
            stmt = "DELETE FROM content_relevant where id > 0"
            result = cur.execute(stmt)
            self.db.commit()
        except Exception as e:
            self.db.rollback()
            pass
        return result

    def insert_multi_relevant(self, data):
        rows = []
        count = 0
        try:
            cur = self.db.cursor()
            for key, value in data.iteritems():
                rows.append((int(key), ",".join(value)))
                count +=1
                if count % 1000 == 0:
                    result = cur.executemany("""INSERT INTO content_relevant (content_id,list_similar_content) VALUES (%s,%s)""", rows)
                    self.db.commit()
                    rows = []
            if rows:
                result = cur.executemany("""INSERT INTO content_relevant (content_id,list_similar_content) VALUES (%s,%s)""", rows)
                self.db.commit()
        except Exception as e:
            self.db.rollback()
            print e
